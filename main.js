const imageP = ["http://pngimg.com/uploads/banana/banana_PNG827.png","https://webiconspng.com/wp-content/uploads/2017/09/Cherry-PNG-Image-67938.png","http://pngimg.com/uploads/pomegranate/pomegranate_PNG8650.png","https://www.freepngimg.com/thumb/mango/1-2-mango-png-thumb.png","http://pluspng.com/img-png/orange-hd-png-download-png-image-orange-png-clipart-744.png","http://pngimg.com/uploads/apple/apple_PNG12503.png","http://purepng.com/public/uploads/large/purepng.com-red-appleappleapplesfruitsweet-1701527180174lrnig.png","http://pngimg.com/uploads/strawberry/strawberry_PNG2636.png"]

const game={countId:1,
result : 0,
 flipcount : 0,
 flipId2 : 0,
 flipId1 : 0,
 c : 30,
 movecount : 0,
 imagesIds : [1, 2, 3, 4, 5, 6, 7, 8],
 imagesIds1 : [101, 102, 103, 104, 105, 106, 107, 108],
 states : {},

 starcount : 0
}
let countFunction;

//constructing mainBoard
function constructmainBoard() {
  shuffleArray(game.imagesIds);
  for (var i = 0; i <= 7; i++) {
    let div = document.createElement("div");
   game.countId = game.imagesIds[i];
    div.setAttribute("id",game.countId);
    div.style.border = "3px solid #03A9F4";
    div.style.textAlign = "center";
    div.addEventListener("click", flipContent);

    maincontainer.appendChild(div);
  }

  shuffleArray(game.imagesIds1);
  for (var i = 0; i <= 7; i++) {
    let div = document.createElement("div");
   game.countId = game.imagesIds1[i];
    div.setAttribute("id",game.countId);
    div.style.border = "3px solid #03A9F4";
    div.style.textAlign = "center";
    div.addEventListener("click", flipContent);
    div.addEventListener("click", play);

    maincontainer.appendChild(div);
  }
}

function timer() {
  countFunction = setInterval(timedCount, 1000);
}


//fliping images conditions
function flipContent(e) {
  if (game.movecount === 0) {
    timer();
  }
  ++game.flipcount;
  ++game.movecount;
  ++game.starcount;

  if (game.movecount == 1) {
    timer();
  }
  var pos = e.target;
  if (game.flipcount > 2) {
    if (pos.parentElement.id === game.flipId2) {
      return;
    }
    game.flipcount = 1;
    if (Math.abs(game.flipId2 - game.flipId1) === 100) {
      ++game.result;
      game.states[game.flipId1] = "found";
      game.states[game.flipId2] = "found";
    } else {
      game.states[game.flipId1] = "false";
      game.states[game.flipId2] = "false";
      document.getElementById(game.flipId2).childNodes[0].remove();
      document.getElementById(game.flipId1).childNodes[0].remove();
    }
  }

  if (game.flipcount === 1) {
    if (game.movecount === 0) {
      timer();
    }
    game.states[pos.id] === "true";
    console.log(pos.id,"  posid")
    game.flipId1 = pos.id;
    if (game.flipId1 === "") {
      game.flipcount = 0;
      return;
    }
    if(pos.id<100){
    var img = document.createElement("img");
    console.log(pos.id,"  ",pos.id-1);
    img.src = imageP[pos.id-1];
    console.log(imageP[pos.id-1])
    document.getElementById(pos.id).appendChild(img);
    }else{
      var img = document.createElement("img");
    console.log(pos.id,"  ",pos.id-100);
    img.src = imageP[pos.id-101];
    document.getElementById(pos.id).appendChild(img);
    }
  }

  if (game.flipcount === 2) {
    if (game.result === 7) ++game.result;
    game.states[pos.id] === "true";
    game.flipId2 = pos.id;
    if (game.flipId2 === "") {
      game.flipcount = 1;
      return;
    }
    if(pos.id<100){
      var img = document.createElement("img");
      console.log(pos.id,"  ",pos.id-1);
      img.src = imageP[pos.id-1];
      console.log(imageP[pos.id-1])
      document.getElementById(pos.id).appendChild(img);
      }else{
        var img = document.createElement("img");
      console.log(pos.id,"  ",pos.id-100);
      img.src = imageP[pos.id-101];
      document.getElementById(pos.id).appendChild(img);
      }
    alert1();
  }
  
  }
  function alert1() {
    if (game.result === 8) {
      starRating();
      delay();
      alert("you have used  "+game.starcount+"  moves     congrats u won the game");
      refreshPage();
    }


  if (game.movecount === 1) {
    window.addEventListener("click", play);
  }
}

function starRating(){
  var star=document.getElementsByClassName("stars_rating");
  var starratingcount=Math.floor(game.starcount/5);
  console.log(starratingcount)
  for(var i=1;i<starratingcount;i++)
  {
    console.log('entered')
    star[0].children[i].classList.toggle("checked");
  }

}

//shuffling images
function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

//timer part
function timedCount() {
  label = document.getElementById("txt");
  label.innerHTML = "";
  label.appendChild(document.createTextNode(game.c));
  game.c -= 1;
  if (game.c < 0) {
    clearInterval(countFunction);
    starRating();
    delay();
    alert("oops time out"+"     you have used  "+game.starcount+"  moves");
    refreshPage();
    }
}

//window loading
window.onload = function() {
  let maincontainer = document.getElementById("maincontainer");
  maincontainer.style.width = "100%";
  var button = document.getElementById("button");
  button.style.width = "60px";
  button.style.height = "20px";

  (function initial() {
    for (var i = 1; i <= 8; i++) {
      game.states[i] = "false";
    }
    for (var i = 1; i <= 8; i++) {
      game.states[100 + i] = "false";
    }
  })();

  constructmainBoard();
};

//audio part
function play() {
  var audio = document.getElementById("audio");
  audio.play();
}

function delay()
{
  setTimeout(function(){},20000);
}

function refreshPage() {
  window.location.href = window.location.href;
}